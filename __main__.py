#!/usr/bin/env python3

import socket
import ssl

from urllib import parse

help_cmd = "h"
quit_cmd = "q"
go_cmd = "g"
back_cmd = "b"

commands = [
    help_cmd,
    quit_cmd,
    go_cmd,
    back_cmd,
]

history = []
page_links = []

def get_cmd():
    while True:
        cmd = input("> ").strip()
        if cmd not in commands:
            print("sorry, not valid. Try", help_cmd, "for help")
        else:
            break
    return cmd

def visit_page(url):
    parsed_url = parse.urlparse(url)
    s = socket.create_connection((parsed_url.netloc, 1965))
    context = ssl.SSSLContext(ssl.PROTOCOL_TLS)
    context.check_hostname = False
    context.verify = ssl.CERT_NONE
    s = context.wrap_socket(s, server_hostname = parsed_url.netloc)
    s.sendall((url + "\r\n").encode("UTF-8"))
    file_object = s.makefile("rb")
    return file_object

def valid_url(url):
    return url

if __name__ == "__main__":
     while True:
        user_cmd = get_cmd()
        if user_cmd == quit_cmd:
            print("goodbye")
            break
        elif user_cmd == go_cmd:
            user_url = input('enter url or link #:')
            while True:
                if user_url.isdigit() and int(user_url) < (page_links.len + 1):
                    visit_page(page_links(int(user_url)))
                elif valid_url(user_url):
                    visit_page(user_url)
        else:
            print('done')

